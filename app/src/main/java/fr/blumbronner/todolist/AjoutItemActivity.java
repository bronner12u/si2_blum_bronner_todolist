package fr.blumbronner.todolist;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class AjoutItemActivity extends AppCompatActivity {

    TodoItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    public void onClickValidItem(View v){
        EditText text_nom = (EditText)findViewById(R.id.nomItem);
        RadioButton bFaible = (RadioButton)findViewById(R.id.rdFaible);
        RadioButton bNormal = (RadioButton)findViewById(R.id.rdNormal);
        RadioButton bImportant = (RadioButton)findViewById(R.id.rdImportant);

        String nomItem = text_nom.getText().toString();

        if (nomItem.isEmpty()){
            Toast.makeText(this, "Indiquez un nom pour votre Item", Toast.LENGTH_LONG).show();
        } else {

            TodoItem.Tags tag;

            if (bFaible.isChecked()) {
                tag = TodoItem.Tags.Faible;
            } else {
                if (bNormal.isChecked()) {
                    tag = TodoItem.Tags.Normal;
                } else {
                    tag = TodoItem.Tags.Important;
                }
            }


            item = new TodoItem(tag, nomItem);
            TodoDbHelper.addItem(item, getBaseContext());

            finish();
        }

    }


}
